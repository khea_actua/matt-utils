
// It is the responsibility of the mongo client consumer to ensure that any necessary windows
// headers have already been included before including the driver facade headers.
#if defined(_WIN32)
#include <winsock2.h>
#include <windows.h>
#endif

#include <cstdlib>
#include <iostream>
#include <chrono>


#include "mongo/client/dbclient.h"
#ifndef verify
#define verify(x) MONGO_verify(x)
#endif

using mongo::BSONArray;
using mongo::BSONArrayBuilder;
using mongo::BSONObj;
using mongo::BSONObjBuilder;
using std::cout;
using std::cerr;
using std::endl;
using timepoint = std::chrono::time_point<std::chrono::system_clock>; // Too long to type!

class TaskResult {
public:

    /**
     * Can't write a stream operator to use this with BSON.. The function would be 
     * \code
     * inline auto operator<<(mongo::BSONObjBuilderValueStream& builder, const TaskResult::TaskStatus& status) -> mongo::BSONObjBuilder&
     * \endcode
     *
     * But to actually stream (see bson-inl.h) we'd need access to private members.
     */
    enum class TaskStatus : int {
        SUCCESS = 0,
        UNINITIALIZED,
        SCHEDULED,
        IN_PROGRESS,
        FAIL,
        CANCELED,
    };
};


auto chronoToDateTime(const timepoint& time) -> mongo::Date_t {
    return mongo::Date_t(std::chrono::system_clock::to_time_t(time)*1000);
}

auto AddRepetition(
    mongo::DBClientBase& conn,
    mongo::BSONElement task_id,
    const timepoint& start_date_time,
    const timepoint& end_date_time,
    int interval) -> void {

    //
    // Embed the repetition in the task document.  This is a one-to-few
    // relationship (one task to a few repetitions)
    auto rep = BSON(mongo::GENOID
        << "start_datetime"          << chronoToDateTime(start_date_time)
        << "end_datetime"            << chronoToDateTime(end_date_time)
        << "interval"                << interval
        << "next_scheduled_datetime" << chronoToDateTime(start_date_time + std::chrono::seconds(interval))
    );

    mongo::BSONElement rep_id;
    if (rep.getObjectID(rep_id)) {
        cout << "Repetition_id = " << rep_id << endl;
    } else {
        cerr << "Could not get repetition id" << endl;
        return;
    }

    auto update = BSON(
        "$push" << BSON("repetitions" << rep));
    conn.update("test.tasks", BSON("_id" << task_id), update);


    //
    // Create the first result for this rep
    // Every result is its own document that can point back to the task and
    // repetition obejct.  This is a one-to-many (one task to many results, one
    // repetition to many results) relationship
    auto task_result = BSON(mongo::GENOID
        << "repetition_id"       << rep_id
        << "task_id"             << task_id
        << "status"              << static_cast<int>(TaskResult::TaskStatus::SCHEDULED)
        << "scheduled_datetime_" << chronoToDateTime(start_date_time)
    );
    try {
        conn.insert("test.results", task_result);
    } catch (mongo::DBException& e) {
        cerr << "Could not insert result: " << e.what() << endl;
    }
}

using TaskRepetitionContainer = std::vector<mongo::BSONElement>;
auto GetTasksToExecute(mongo::DBClientBase& conn, const timepoint& now) -> TaskRepetitionContainer {
    TaskRepetitionContainer ret;

    auto query = BSON("repetitions.next_scheduled_datetime" << BSON("$lte" << chronoToDateTime(now)));

    auto cursor = conn.query("test.tasks", query);
    if (!cursor.get()) {
        cerr << "query failure" << endl;
    }

    while (cursor->more()) {
        mongo::BSONObj obj = cursor->next();

        mongo::BSONElement id;
        obj.getObjectID(id);
        ret.emplace_back(id);
    }

    return ret;
}


auto main(int, char**) -> int {
    mongo::client::GlobalInstance instance;
    if (!instance.initialized()) {
        cout << "failed to initialize the client driver: "
                  << instance.status() << endl;
        return EXIT_FAILURE;
    }

    std::string uri = "mongodb://localhost:27017";
    std::string errmsg;

    mongo::ConnectionString cs = mongo::ConnectionString::parse(uri, errmsg);
    if (!cs.isValid()) {
        cout << "Error parsing connection string " << uri << ": " << errmsg
                  << endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<mongo::DBClientBase> conn{cs.connect(errmsg)};

    const char* ns = "test.tasks";
    conn->dropCollection(ns);
    conn->dropCollection("test.results");

    auto now = std::chrono::system_clock::now();

    //
    // Stockpile events
    {
    BSONObjBuilder task;
        task.genOID();
        task.append("start_datetime",
                    chronoToDateTime(now + std::chrono::hours(24)));

        task.append("end_date",
                    chronoToDateTime(now + std::chrono::hours(24*5)));

        BSONArrayBuilder stockpiles;
        stockpiles.append(BSON( "boundary" << "sp1" << "label" << "SP 1" << "density" << 12.4 ));
        stockpiles.append(BSON( "boundary" << "sp2" << "label" << "SP 2" << "density" << 22.4 ));

        BSONObjBuilder event;
        event.append("type", "stockpile_monitor");
        event.append("format", "csv");
        event.append("scan_duration", 30);
        event.appendArray("stockpiles", stockpiles.arr());

        task.append("event", event.obj());

        conn->insert(ns, task.obj());
    }


    {
        BSONArray stockpiles = BSON_ARRAY(BSON("boundary" << "sp3" << "label" << "SP 3" << "density" << 1.4));

        BSONObjBuilder event;
        event.append("type", "stockpile_monitor");
        event.append("format", "csv");
        event.append("scan_duration", 50);
        event.appendArray("stockpiles", stockpiles);

        BSONObj task =
            BSON(mongo::GENOID
                        << "start_datetime" << chronoToDateTime(now + std::chrono::hours(24 * 2))
                        << "end_datetime" << chronoToDateTime(now + std::chrono::hours(24 * 6))
                        << "event" << event.obj());

        conn->insert(ns, task);
    }


    //
    // Bench mapper events
    {
        BSONObj task =
            BSON(mongo::GENOID
                        << "start_datetime" << chronoToDateTime(now + std::chrono::hours(24 * 2))

                        << "end_datetime" << chronoToDateTime(now + std::chrono::hours(24 * 6))
                        << "event" << BSON(
                            "type" << "bench_mapper"
                            << "format" << "csv"
                            << "scan_duration" << 50));

        conn->insert(ns, task);
    }


    // Find one
    auto res = conn->findOne(ns, BSON("event.type" << "bench_mapper"));
    cout << res.isEmpty() << "\t" << res.jsonString() << endl;

    mongo::BSONElement obj_id;
    res.getObjectID(obj_id);
    cout << "task_id = " << res.getField("_id") << " -> "
         << res.getStringField("_id")
         << " -> " << obj_id
         << endl;

    // Add a rep
    AddRepetition(*conn, obj_id, now, now+std::chrono::hours(48), 60);

    // Get a list of tasks to execute
    // Arbitrary future time
    auto task_ids_to_execute = GetTasksToExecute(*conn, now + std::chrono::hours(50));
    if (task_ids_to_execute.size()) {
        cout << "Executing tasks IDs: ";
        for (const auto& task_id : task_ids_to_execute) {
            cout << task_id << " ";
        }
        cout << endl;
    } else {
        cout << "Could not find tasks to execute" << endl;
    }

    return 0;
}

/* vim: set ts=4 sw=4 sts=4 expandtab ffs=unix : */
