#ifndef SCHEDULER_H_EZ7RKJP9
#define SCHEDULER_H_EZ7RKJP9

#include "cpprest/http_listener.h"

#include "models/BenchMapperTask.h"
#include "models/FragmentationTask.h"
#include "models/StockpileMonitorTask.h"
#include "models/Task.h"

#include "Reply.h"

namespace OPAL2
{
namespace web
{
namespace REST
{
namespace v1
{

class Scheduler
{
public:
    Scheduler() {}
    Scheduler(utility::string_t url);

    auto open() -> pplx::task<void>  { return m_listener.open(); }
    auto close() -> pplx::task<void> { return m_listener.close(); }

protected:

    auto handle_get(::web::http::http_request message) -> void;
    auto handle_post(::web::http::http_request message) -> void;

    ::web::http::experimental::listener::http_listener m_listener;
};

auto ValidateTask(const ::web::json::object& request, Reply::ErrorMessages& errors) -> bool;
auto ValidateBenchMapperTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::unique_ptr<BenchMapperTask>&& task) -> bool;
auto ValidateStockpileTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::unique_ptr<StockpileMonitorTask>&& task) -> bool;
auto ValidateFragmentationTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::unique_ptr<FragTask>&& task) -> bool;

} /* v1 */
} /* REST */
} /* web */
} /* OPAL2 */

#endif /* end of include guard: SCHEDULER_H_EZ7RKJP9 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
