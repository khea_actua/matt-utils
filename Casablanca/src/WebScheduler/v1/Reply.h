#ifndef REPLY_H_8PBR5VEH
#define REPLY_H_8PBR5VEH

#include <locale>
#include <string>
#include <vector>

#include "cpprest/json.h"
#include "SerializableToJSON.h"

namespace OPAL2
{
namespace web
{
namespace REST
{
namespace v1
{

/**
 * Custom format reply.  Contains a result code, message, and payload (json
 * data.  To be wrapped around all "succesful" (where HTTP codes wouldn't be
 * appropriate) replies.
 *
 * For example, if the user queries a list, then that list will be returned in
 * this reply, if the user queries an invalid URL, or asked for an invalid
 * entity, then the http code for NOT_FOUND (300 or something?) can be
 * returned.  If however, a post is attempted but is missing information, this
 * reply can be returned with an error result code.
 */
class Reply : public OPAL2::web::REST::SerializableToJSON {
public:
	using ErrorMessages = std::vector<std::string>;

	enum class ResultCode : int {
		OK = 0,
		Err = -1,
	};

	/** Fully define the reply */
	Reply(const ResultCode& code, const std::string& msg, const OPAL2::web::REST::SerializableToJSON& data);

	/** Default the message to being blank */
	Reply(const ResultCode& code, const OPAL2::web::REST::SerializableToJSON& data);

	/** Default the code to OK */
	Reply(const std::string& msg, const OPAL2::web::REST::SerializableToJSON& data);

	/** OK reply with empty message and an OK code */
	Reply(const OPAL2::web::REST::SerializableToJSON& data);

	/** OK reply with empty message and an OK code */
	Reply(const ResultCode& code, const std::string& msg);

	/** Trivial destructor */
	~Reply() {}

	virtual auto ToJSON() const -> ::web::json::value;

	/** Concatenate error messages into a string */
	static auto Concat(const ErrorMessages& errors) -> std::string;

	auto msg() const -> ::utility::string_t {
		return ::utility::conversions::to_string_t(msg_);
	};

protected:
	std::string msg_;

	ResultCode code_ = ResultCode::OK;

	::web::json::value data_;
};

} /* v1 */
} /* REST */
} /* web */
} /* OPAL2 */

#endif /* end of include guard: REPLY_H_8PBR5VEH */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
