#include "Scheduler.h"

#include <string>
#include <locale>

#include <boost/algorithm/string/case_conv.hpp>

#include "cpprest/json.h"

#include "v1/Reply.h"

#include "database/db.h"

namespace OPAL2
{
namespace web
{
namespace REST
{
namespace v1
{

using namespace ::web;
using namespace ::web::http;
using namespace ::utility;
using namespace ::utility::conversions;
using namespace ::web::http::experimental::listener;
using namespace std;
using namespace OPAL2::web;

Scheduler::Scheduler(utility::string_t url) : m_listener(url) {
	m_listener.support(methods::GET,  std::bind(&Scheduler::handle_get, this, std::placeholders::_1));
	m_listener.support(methods::POST, std::bind(&Scheduler::handle_post, this, std::placeholders::_1));
}

void Scheduler::handle_get(http_request message)
{

	auto paths =
	    http::uri::split_path(http::uri::decode(message.relative_uri().path()));

	// Connect to the db
	auto& db = OPAL2::web::SimpleDb::GetInstance();

	TaskCollection tasks;
	if (paths.size()) {
		std::string path{to_utf8string(paths.front())};
		boost::to_lower(path);

		if (path == "benchmapper") {
			tasks = db.FindAll(Task::TaskType::BENCHMAPPER);
		} else if (path == "stockpilemonitor") {
			tasks = db.FindAll(Task::TaskType::STOCKPILE_MONITOR);
		} else if (path == "fragmentation") {
			tasks = db.FindAll(Task::TaskType::FRAGMENTATION);
		} else {
            message.reply(status_codes::BadRequest, Reply(Reply::ResultCode::Err, "Invalid task type").ToJSON());
			return;
		}
	} else {
		tasks = db.FindAll();
	}

	message.reply(status_codes::OK, Reply("Requested tasks", tasks).ToJSON());
};

auto ValidateTask(const ::web::json::object& request, Reply::ErrorMessages& errors, Task::DurationTime& scan_duration, Task::FileFormat& file_format_id) -> bool {
	if (request.find(U("scan_duration")) == request.end())
		errors.push_back("Must specify scan duration!");
    if (request.find(U("start_datetime")) == request.end())
		errors.push_back("Must specify start date time!");
    if (request.find(U("end_date")) == request.end())
        errors.push_back("Must specify start date time!");
    if (request.find(U("file_format")) == request.end())
        errors.push_back("Must specify a file format!");
    if (request.find(U("scan_duration")) == request.end())
		errors.push_back("Must specify scan duration!");

	//
	// Validate inputs
	json::value val;

	val = (*request.find(U("scan_duration"))).second;
	if (!val.is_integer()) {
		errors.push_back("Scan duration must be a positive integer!");
		return false;
	}
	if (val.as_integer() < 0) {
		errors.push_back("Scan duration must be a positive!");
		return false;
	}
	scan_duration = val.as_integer();

    auto fmt_string = boost::to_lower_copy(::utility::conversions::to_utf8string((*request.find(U("file_format"))).second.as_string()));
	if (0 == fmt_string.compare("csv")) {
		file_format_id = Task::FileFormat::CSV;
	} else if (0 == fmt_string.compare("binary")) {
		file_format_id = Task::FileFormat::BINARY;
	} else {
		file_format_id = Task::FileFormat::CSV;
	}

	return 0 == errors.size();
}

auto ValidateBenchMapperTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::shared_ptr<BenchMapperTask>&& task) -> bool {
	json::value val;

	// These will be set by ValidateTask
	Task::DurationTime scan_duration;
	Task::FileFormat file_format_id;

	// Validate general task data
	ValidateTask(request, errors, scan_duration, file_format_id);
	if (errors.size()) return false;

	//
	// If we've made it here, try to create the object.
	// Typically we'd use setters() which would apply another layer of
	// validation, but keeping it simple for now

	task = std::make_shared<BenchMapperTask>();

	try {
        task->start_datetime_ = to_utf8string((*request.find(to_string_t("start_datetime"))).second.as_string()); // should be a date
        task->end_date_       = to_utf8string((*request.find(to_string_t("end_date"))).second.as_string()); // should be a date
		task->scan_duration_  = scan_duration;
		task->file_format_    = file_format_id;
	} catch (std::exception& e) {
		errors.push_back(e.what());
		return false;
	}

	return true;
}

auto ValidateStockpileTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::shared_ptr<StockpileMonitorTask>&& task) -> bool {
	json::value val;

	// These will be set by ValidateTask
	Task::DurationTime scan_duration = 0;
	Task::FileFormat file_format_id;

	// Validate general task data
	ValidateTask(request, errors, scan_duration, file_format_id);
	if (errors.size()) return false;

	//
	// Build the stockpile_map
	// fake data for now ...


	//
	// If we've made it here, try to create the object.
	// Typically we'd use setters() which would apply another layer of
	// validation, but keeping it simple for now

	task = std::make_shared<StockpileMonitorTask>();

	try {
        task->start_datetime_ = to_utf8string((*request.find(to_string_t("start_datetime"))).second.as_string()); // should be a date
        task->end_date_ = to_utf8string((*request.find(to_string_t("end_date"))).second.as_string()); // should be a date
		task->scan_duration_  = scan_duration;
		task->file_format_    = file_format_id;

		task->AddStockpile("gravel",       2000, "gravel.pcd");
		task->AddStockpile("pure gold",    8000, "gold.pcd");
		task->AddStockpile("Cat pictures",   10, "catz.pcd");
	} catch (std::exception& e) {
		errors.push_back(e.what());
		return false;
	}

	return true;
}

auto ValidateFragmentationTask(const ::web::json::object& request, Reply::ErrorMessages& errors, std::shared_ptr<FragTask>&& task) -> bool {
	json::value val;

	// These will be set by ValidateTask
	Task::DurationTime scan_duration;
	Task::FileFormat file_format_id;

	// Validate general task data
	ValidateTask(request, errors, scan_duration, file_format_id);
	if (request.find(U("label")) == request.end())
		errors.push_back("Must specify boundary label");
	if (request.find(U("boundary_file")) == request.end())
		errors.push_back("Must specify boundary file");
	if (errors.size()) return false;


	//
	// If we've made it here, try to create the object.
	// Typically we'd use setters() which would apply another layer of
	// validation, but keeping it simple for now

	task = std::make_shared<FragTask>();

	try {
		task->start_datetime_ = to_utf8string((*request.find(to_string_t("start_datetime"))).second.as_string()); // should be a date
		task->end_date_ = to_utf8string((*request.find(to_string_t("end_date"))).second.as_string()); // should be a date
		task->scan_duration_ = scan_duration;
		task->file_format_ = file_format_id;

		task->label_          = to_utf8string((*request.find(to_string_t("label"))).second.as_string());
		task->boundary_file_  = to_utf8string((*request.find(to_string_t("boundary_file"))).second.as_string());
	} catch (std::exception& e) {
		errors.push_back(e.what());
		return false;
	}

	return true;
}


auto Scheduler::handle_post(http_request message) -> void {

	auto headers = message.headers();
	cout << "\nPOST Headers:";
	for (const auto& header : headers)
		cout << to_utf8string(header.first) << "=" << to_utf8string(header.second) << "\n";
	cout << endl;

	json::value value;
	message.extract_json()
		 .then([&](pplx::task<json::value> previousTask) {
			 try {
				 const auto& v = previousTask.get();
				 value = v;

			 } catch (const http_exception& e) {
				 // Print error.
				 std::wostringstream ss;
				 ss << e.what() << std::endl;
				 std::wcout << ss.str();
			 }
		 })
		 .wait();
	auto request_data = value.as_object();

	//
	// Determine what type of event the user is attempting to schedule by
	// looking at the path

	auto paths =
	    http::uri::split_path(http::uri::decode(message.relative_uri().path()));
	if (paths.empty()) {
        message.reply(status_codes::BadRequest, Reply(Reply::ResultCode::Err, "Must specify task type to schedule").ToJSON());
		return;
	} else if (paths.size() > 1) {
		message.reply(status_codes::BadRequest);
		return;
	}

	std::string path{::utility::conversions::to_utf8string(paths.front())};
	boost::to_lower(path);

	std::shared_ptr<Task> task = nullptr;
	// Shared pointer so that we can persist them in the DB (std::vector can't save derived classes)
	std::shared_ptr<BenchMapperTask> bm_task;
	std::shared_ptr<StockpileMonitorTask> sp_task;
	std::shared_ptr<FragTask> frag_task;
	Reply::ErrorMessages errors;
	if (path == "benchmapper") {
		if (ValidateBenchMapperTask(request_data, errors, std::move(bm_task))) task = bm_task;
	} else if (path == "stockpilemonitor") {
		if (ValidateStockpileTask(request_data, errors, std::move(sp_task))) task = sp_task;
	} else if (path == "fragmentation") {
		if (ValidateFragmentationTask(request_data, errors, std::move(frag_task))) task = frag_task;
	} else {
        message.reply(status_codes::BadRequest, Reply(Reply::ResultCode::Err, "Invalid task type, valid types are 'BenchMapper', 'StockpileMonitor', and 'Fragmentation'").ToJSON());
		return;
	}
	if (task == nullptr) {
        message.reply(status_codes::BadRequest, Reply(Reply::ResultCode::Err, Reply::Concat(errors)).ToJSON());
		return;
	}

	// Connect to the db
	auto& db = OPAL2::web::SimpleDb::GetInstance();
	db.Save(task);

	// Success!
	message.reply(status_codes::OK, Reply("Task succesfully created", *task).ToJSON());

	return;
};

} /* v1 */
} /* REST */
} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
