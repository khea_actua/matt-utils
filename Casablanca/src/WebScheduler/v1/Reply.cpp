#include "Reply.h"

#include <string>
#include <locale>

#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{
namespace REST
{
namespace v1
{

using namespace OPAL2::web::REST;

Reply::Reply(const ResultCode& code, const SerializableToJSON& data)
	: Reply(code, "", data) {}

Reply::Reply(const SerializableToJSON& data)
	: Reply(ResultCode::OK, "", data) {}

Reply::Reply(const std::string& msg, const SerializableToJSON& data)
	: Reply(ResultCode::OK, msg, data) {}

Reply::Reply(const ResultCode& code, const std::string& msg)
	: code_(code)
	, msg_(msg)
	, data_(::web::json::value::Null) {}

Reply::Reply(const ResultCode& code, const std::string& msg, const SerializableToJSON& data)
	: msg_(msg)
	, code_(code)
	, data_(data.ToJSON()) {}

auto Reply::ToJSON() const -> ::web::json::value {
	using namespace ::web;
	auto result = json::value::object();

	result[U("code")] = json::value::number(static_cast<int>(code_));
	result[U("msg")]  = json::value::string(msg());
    result[U("data")] = data_;
	return result;
}

auto Reply::Concat(const ErrorMessages& errors) -> std::string {
	decltype(errors.size()) c = 0;

	std::string msg;
	for (const auto& error : errors) {
		if (c < errors.size())
			msg = msg + error + ", ";
		else
			msg = msg + error;
	}
	return msg;
};


} /* v1 */
} /* REST */
} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
