#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <random>

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#include "cpprest/json.h"
#include "cpprest/http_listener.h"
#include "cpprest/uri.h"
#include "cpprest/asyncrt_utils.h"

#include "v1/Scheduler.h"

#include "database/db.h"

using namespace web;
using namespace web::http;
using namespace utility;
using namespace web::http::experimental::listener;

#include <iostream>
#include <string>
using namespace std;

namespace OPAL2
{
namespace web
{
namespace REST
{

std::unique_ptr<OPAL2::web::REST::v1::Scheduler> g_httpScheduler;
auto on_initialize(const string_t &address) -> void {
	// Build our listener's URI from the configured address and the hard-coded
	// path "blackjack/dealer"

	uri_builder uri(address);
	uri.append_path(U("v1/schedule"));

	auto addr = uri.to_uri().to_string();
	g_httpScheduler = std::unique_ptr<OPAL2::web::REST::v1::Scheduler>(new OPAL2::web::REST::v1::Scheduler(addr));
	g_httpScheduler->open().wait();

	ucout << utility::string_t(U("Listening for requests at: ")) << addr
	      << std::endl;

	return;
}

auto on_shutdown() -> void {
	g_httpScheduler->close().wait();
	return;
}

} /* REST */
} /* web */
} /* OPAL2 */

#ifdef _WIN32
auto wmain(int argc, wchar_t *argv[]) -> int
#else
auto main(int argc, char *argv[]) -> int
#endif
{
	using namespace OPAL2::web::REST;

	utility::string_t port = U("34568");
	if (argc == 2) {
		port = argv[1];
	}

	// Create a db connection
	OPAL2::web::SimpleDb::GetInstance();

	utility::string_t address = U("http://localhost:");
	address.append(port);

	on_initialize(address);
	std::cout << "Press ENTER to exit." << std::endl;

	std::string line;
	std::getline(std::cin, line);

	on_shutdown();
	return 0;
}

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
