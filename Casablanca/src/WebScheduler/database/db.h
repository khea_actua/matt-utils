#include <memory>
#include <mutex>
#include <sstream>
#include <vector>

#include "models/Task.h"

#ifndef DB_H_0CCSPW5Q
#define DB_H_0CCSPW5Q

namespace OPAL2
{
namespace web
{

/**
 * Extremely simple singleton data store.  Entire point is just to save objects in the Casablanca tests.
 */
class SimpleDb
{
public:

	/** Returns the instance of Logger */
	static SimpleDb& GetInstance();

	SimpleDb(const SimpleDb&) = delete;
	void operator=(SimpleDb const&) = delete;

	/** Save a copy of the object */
	auto Save(std::shared_ptr<Task> task) -> void;
	auto FindAll() const -> TaskCollection;
	auto FindAll(const Task::TaskType& type) const -> TaskCollection;

protected:

	SimpleDb() {};

	/** Calls UnregisterAllObservers all observers */
	~SimpleDb() {};

	/** Object store */
	TaskCollection store_;
};

} /* web */
} /* OPAL2 */

#endif /* end of include guard: DB_H_0CCSPW5Q */

/* vim: set ts=4 sw=4 sts=0 noet ffs=unix : */
