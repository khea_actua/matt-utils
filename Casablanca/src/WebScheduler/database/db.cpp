#include "db.h"

namespace OPAL2
{
namespace web
{

SimpleDb& SimpleDb::GetInstance() {
    static SimpleDb instance{};
    return instance;
}

auto SimpleDb::Save(std::shared_ptr<Task> task) -> void {
	// push_back should copy task
	store_.push_back(task);
}

auto SimpleDb::FindAll() const -> TaskCollection {
	// Just return everything.
	return store_;
}

auto SimpleDb::FindAll(const Task::TaskType& type) const -> TaskCollection {
	TaskCollection ret;
	for (const auto& task : store_)
		if (type == task->task_type_) ret.push_back(task);
	return ret;
}

} /* web */
} /* OPAL2 */

/* vim: set ts=4 sw=4 sts=0 noet ffs=unix : */
