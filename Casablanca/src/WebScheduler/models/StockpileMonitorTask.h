#ifndef STOCKPILEMONITORTASK_H_SKM3DYR0
#define STOCKPILEMONITORTASK_H_SKM3DYR0

#include "models/Task.h"

#include <map>

#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{

class StockpileMonitorTask : public Task {
public:
	// Parameters = (density, boundary file)
	using StockpileParams = std::tuple<double, std::string>;

	StockpileMonitorTask();
	~StockpileMonitorTask() {};

	auto ToJSON() const -> ::web::json::value;

	auto AddStockpile(const std::string& label, const double& density, const std::string& boundary_file) -> void;

	// Map of stockpiles, using the label as a key
	std::map<std::string, StockpileParams> stockpile_map_;
protected:
	/**
	 * Represent the stockpile map in JSON
	 */
	auto StockpileMapToJSON() const -> ::web::json::value;
};

} /* web */
} /* OPAL2 */

#endif /* end of include guard: STOCKPILEMONITORTASK_H_SKM3DYR0 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
