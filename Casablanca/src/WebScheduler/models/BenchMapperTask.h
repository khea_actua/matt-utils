#ifndef BenchMapperTask_H_CEW0HU5O
#define BenchMapperTask_H_CEW0HU5O

#include "models/Task.h"

namespace OPAL2
{
namespace web
{

class BenchMapperTask : public Task {
public:
	BenchMapperTask();
	~BenchMapperTask() {};

	auto ToJSON() const -> ::web::json::value;
};

} /* web */
} /* OPAL2 */

#endif /*  : public TaskIdend of include guard: BenchMapperTask_H_CEW0HU5O */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
