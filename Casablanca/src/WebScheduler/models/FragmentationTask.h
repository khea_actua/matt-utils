#ifndef FRAGMENTATIONTASK_H_KRWMPCKR
#define FRAGMENTATIONTASK_H_KRWMPCKR

#include "models/Task.h"

namespace OPAL2
{
namespace web
{

class FragTask : public Task {
public:
	FragTask();
	~FragTask() {};

	auto ToJSON() const -> ::web::json::value;

	/** Label for area of interest */
	std::string label_;

	/** Boundary file descripting area of interest */
	std::string boundary_file_;
};

} /* web */
} /* OPAL2 */

#endif /* end of include guard: FRAGMENTATIONTASK_H_KRWMPCKR */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
