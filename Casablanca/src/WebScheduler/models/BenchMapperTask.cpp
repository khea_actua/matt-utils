#include "models/BenchMapperTask.h"

#include <iostream>
#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{

BenchMapperTask::BenchMapperTask()
	: Task(Task::TaskType::BENCHMAPPER)
{
    std::cout << "Created new Bench Mapper event (" << Id() << ")" << std::endl;
}

auto BenchMapperTask::ToJSON() const -> ::web::json::value {
	using namespace ::web;
	auto result = Task::ToJSON();

	auto sub_event = json::value::object();
	result[U("benchmapper_event")] = sub_event;
	return result;
};


} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
