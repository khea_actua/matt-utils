#include "models/Task.h"

#include <chrono>
#include <ctime>
#include <functional>
#include <random>

namespace OPAL2
{
namespace web
{

using namespace utility::conversions;

const Task::TaskId Task::kUnInitializedId = 0;
const std::map<Task::FileFormat, std::string> Task::FormatMap = Task::CreateFormatMap();

Task::Task(TaskType type)
	: event_created_datetime_(std::chrono::system_clock::now())
	, task_type_(type)
{}

auto Task::GenId() -> TaskId {
	auto rand_id=std::bind(std::uniform_int_distribution<>{100,999}, std::default_random_engine{});
	return static_cast<TaskId>(rand_id());
}

auto Task::TaskTypeToString() const -> std::string {
	return TaskTypeToString(task_type_);
}
auto Task::TaskTypeToString(TaskType type) -> std::string {
	switch (type) {
		case Task::TaskType::STOCKPILE_MONITOR:
			return "stockpile_monitor";
		case Task::TaskType::BENCHMAPPER:
			return "benchmapper";
		case Task::TaskType::FRAGMENTATION:
			return "fragmentation";
		default:
			return "unknown task type";
	}
};

auto Task::FileFormatName(Task::FileFormat format_id) -> std::string {
    try {
        return Task::FormatMap.at(format_id);
    } catch (const std::out_of_range&) {
        return "Unknown";
    }
}

auto Task::ToJSON() const -> ::web::json::value {
	using namespace ::web;
	auto result = json::value::object();

	result[U("id")] = json::value::number(Id());
	result[U("event_created")] = json::value::string(to_string_t(TimeToString(event_created_datetime_)));
	result[U("task_type")] = json::value::string(to_string_t(TaskTypeToString()));
	result[U("scan_duration")] = json::value::number(scan_duration_);
	result[U("file_format")] = json::value::string(to_string_t(FileFormatName(file_format_)));

	return result;
};

auto TimeToString(std::chrono::time_point<std::chrono::system_clock> t) -> std::string {
	auto timestamp = std::chrono::system_clock::to_time_t(t);
	std::string timestamp_string{std::ctime(&timestamp)};
	timestamp_string = timestamp_string.substr(0, timestamp_string.length()-1);
	return timestamp_string;
}

auto TaskCollection::ToJSON() const -> ::web::json::value {
	auto arr = ::web::json::value::array();

	int indx = 0;
	for (const auto& task : *this) {
		arr[indx++] = task->ToJSON();
	}
	return arr;
}

} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
