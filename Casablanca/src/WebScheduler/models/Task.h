#ifndef Task_CPP_KMRDIOB6
#define Task_CPP_KMRDIOB6

#include <cstddef>
#include <map>
#include <string>

#include "cpprest/json.h"
#include "SerializableToJSON.h"

namespace OPAL2
{
namespace web
{

class Task : public REST::SerializableToJSON {
public:
	using DurationTime = std::size_t;
	using TaskId = int;

	/** Invalid/unitialized ID */
	static const TaskId kUnInitializedId;

	enum class TaskType : int {
		BENCHMAPPER = 0,
		FRAGMENTATION,
		STOCKPILE_MONITOR
	};

	enum class FileFormat : int {
		CSV = 0,
		BINARY,
	};


	Task(TaskType type);
	~Task(){};

	virtual auto ToJSON() const -> ::web::json::value = 0;

	// Should be protected, but public for convinience right now
	DurationTime scan_duration_ = 0;
	std::string start_datetime_;
	std::string end_date_;
	FileFormat file_format_;

	std::chrono::time_point<std::chrono::system_clock> event_created_datetime_;

	TaskType task_type_;

	auto TaskTypeToString() const -> std::string;
	static auto TaskTypeToString(TaskType type) -> std::string;

	static auto CreateFormatMap() -> std::map<FileFormat, std::string> {
		return std::map<FileFormat, std::string>{
		    {FileFormat::CSV, "Comma Separated Values (CSV)"},
		    {FileFormat::BINARY, "Binary"}};
	}
	static const std::map<FileFormat, std::string> FormatMap;

	/**
	 * Simple getter to get the file format name, so I don't always have to
	 * access the map with try/catch statements, etc
	 */
	static auto FileFormatName(FileFormat format_id) -> std::string;

	/**
	 * Getter for ID
	 */
	auto Id() const -> TaskId { return id_; }

protected:
	/** Generate random ID **/
	static auto GenId() -> TaskId;

	/** Entity ID.  Initialized to a random number for this test program */
	const TaskId id_ = GenId();

};

class TaskCollection : public REST::SerializableToJSON
							, public std::vector<std::shared_ptr<Task>>
{
	virtual auto ToJSON() const -> ::web::json::value;
};


/**
 * Small helper function to convert time points to strings.
 */
auto TimeToString(std::chrono::time_point<std::chrono::system_clock> t) -> std::string;

} /* web */
} /* OPAL2 */

#endif /* end of include guard: Task_CPP_KMRDIOB6 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
