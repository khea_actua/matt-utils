#include "models/FragmentationTask.h"

#include <iostream>
#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{

using namespace utility::conversions;

FragTask::FragTask()
	:Task(Task::TaskType::FRAGMENTATION) {}

auto FragTask::ToJSON() const -> ::web::json::value {
	using namespace ::web;
	auto result = Task::ToJSON();

	auto sub_event = json::value::object();
	sub_event[U("label")]            = json::value::string(to_string_t(label_));
	sub_event[U("boundary_file")]    = json::value::string(to_string_t(boundary_file_));

	result[U("fragmentation_event")] = sub_event;
	return result;
};

} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */
