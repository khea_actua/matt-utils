#include "models/StockpileMonitorTask.h"

#include <iostream>
#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{

using namespace utility::conversions;

StockpileMonitorTask::StockpileMonitorTask()
	: Task(Task::TaskType::STOCKPILE_MONITOR)
{
    std::cout << "Created new Bench Mapper event (" << Id() << ")" << std::endl;
}

auto StockpileMonitorTask::StockpileMapToJSON() const -> ::web::json::value {
	using namespace ::web::json;
	auto sp_array = value::array();

	int indx = 0;
	for (const auto& stockpile : stockpile_map_) {
		auto sp_json = value::object();
		sp_json[U("label")]         = value::string(to_string_t(stockpile.first));
		sp_json[U("density")] = value::number(std::get<0>(stockpile.second));
		sp_json[U("boundary_file")] = value::string(to_string_t(std::get<1>(stockpile.second)));

		sp_array[indx++] = sp_json;
	}
	return sp_array;
};

auto StockpileMonitorTask::ToJSON() const -> ::web::json::value {
	using namespace ::web;
	auto result = Task::ToJSON();

	auto sub_event = json::value::object();
	sub_event[U("stockpiles")] = StockpileMapToJSON();

	result[U("stockpilemonitor_event")] = sub_event;
	return result;
};

auto StockpileMonitorTask::AddStockpile(const std::string& label, const double& density, const std::string& boundary_file) -> void {
	stockpile_map_.emplace(label, std::make_tuple(density, boundary_file));
}

} /* web */
} /* OPAL2 */

/* vim: set ts=3 sw=3 sts=0 noet ffs=unix,dos : */