#ifndef SERIALIZABLETOJSON_H_K7Z0JIDR
#define SERIALIZABLETOJSON_H_K7Z0JIDR

#include "cpprest/json.h"

namespace OPAL2
{
namespace web
{
namespace REST
{

/** Interface for objects that can be serialized to JSON */
class SerializableToJSON {
public:
	virtual auto ToJSON() const -> ::web::json::value = 0;
};

} /* REST */
} /* web */
} /* OPAL2 */

#endif /* end of include guard: SERIALIZABLETOJSON_H_K7Z0JIDR */

/* vim: set ts=4 sw=4 sts=0 noet ffs=unix,dos : */
