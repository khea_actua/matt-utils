#include <iostream>
#include <thread>
#include <mutex>
#include <utility>
#include <vector>

#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

using namespace std;

bool verbose = false;

class Executor {
public:
    Executor() {
        count++;
        cout << "Created executor " << count << endl;
    }
    ~Executor() {
        cout << "Destroying executor " << count << endl;
        count--;
    }

    auto Go(boost::promise<int>& p, int start) -> void {
        auto sum = start;
        auto id = boost::this_thread::get_id();
        if (one_at_a_time_.try_lock()) {
            cout << id << " -> starting with start = " << start << endl;
            if (verbose) cout << id << " -> Go: got mutex" << endl;
            for (auto i = 0; i < 4; ++i) {
                sum += i;
                boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            }
            if (verbose) cout << id << " -> Go: Releasing mutex, returning " << start << " -> " << sum << endl;
            one_at_a_time_.unlock();
        } else {
            cout << id << " -> Go: Could not lock mutex, returning " << start << " -> " << sum << endl;
        }
        p.set_value(sum);
    }

    /** How many executors we have */
    static int count;

private:
    /** Execution mutex */
    mutex one_at_a_time_;
};

int Executor::count = 0;

auto hammerMutex(Executor& worker, int start) -> int {
    boost::promise<int> p;
    boost::thread t(boost::bind(&Executor::Go, &worker, boost::ref(p), start));
    auto val = p.get_future().get();
    cout << "Promise result " << start << " -> " << val << endl;
    return val;
}

int main(int argc, char *argv[]) {

    if (argc > 1) verbose = true;

    Executor worker;

    // Call normally, these will launch the functions with mutices
    hammerMutex(worker, 0);
    hammerMutex(worker, 10);

    // Now, attempt to lunch a bunch of threads asynchronously
    cout << "Launching hammer function in threads.." << endl;
    for (int i=0; i<50; i++) {
        if (verbose) cout << ".. Launching thread number " << i << endl;
        boost::thread h(hammerMutex, boost::ref(worker), i*10);
        boost::this_thread::sleep_for(boost::chrono::milliseconds(50));
        h.detach();
    }
}


/* vim: set ts=4 sw=4 expandtab : */
