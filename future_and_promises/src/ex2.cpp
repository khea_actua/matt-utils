///////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved. //
///////////////////////////////////////////////////////////////////////////////

/**
 * Example script to immitate calling the API to do work that'll interface with
 * the scanners.  This example has multiple threads requesting work be done,
 * and the work is protected by a mutex.  When the worker cannot get a mutex,
 * it worker returns false, but when it can, it performs the work and returns a
 * result.
 */

#include <iostream>
#include <thread>
#include <mutex>
#include <utility>
#include <vector>

#define BOOST_THREAD_PROVIDES_FUTURE_CONTINUATION
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

// Shortcut
using namespace std;

// Global variable used to increase verbosity, set to true by having any
// program arguments
bool verbose = false;

/**
 * Example worker class.
 *
 * Call the ::Go(start_val) member to mimic work.  This function is wrapped
 * with a mutex, if it can grab the lock, it will return a Result{true, int*},
 * if not, it'll return Result{false, nullptr}.
 *
 * This class is used as a singleton in this example script by only ever
 * instantiating one copy of it and passing it around by reference.
 *
 * Essentially, this class is here to mimic the 3DRi API, which it self is a
 * singleton.
 */
class Executor {
public:
    using Result = pair<bool, int*>;

    Executor() { cout << "Created executor" << endl; }
    ~Executor() { cout << "Destroying executor" << endl; }

    /**
     * @brief Try to get a mutex lock and perform work. (mostly sleep though...)
     *
     * @param[in] start Start value, not really used, but helpful as an
     *                  indentifier sometimes
     * @return {true, int*} if it managed to get the lock, {false, nullptr}
     *          otherwise
     */
    auto Go(int start) -> Result {
        auto id = boost::this_thread::get_id();
        Result result{false, nullptr};
        if (one_at_a_time_.try_lock()) {
            if (verbose) cout << id << " -> Go: got mutex, start = " << start << endl;
            for (auto i = 0; i < 4; ++i) {
                boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            }

            auto val = new int{5};

            result.first = true;
            result.second = val;

            if (verbose) cout << id << " -> Go: Releasing mutex" << endl;
            one_at_a_time_.unlock();
        } else {
            if (verbose) cout << id << " -> Go: Could not lock mutex, returning from worker starting from " << start << endl;
        }
        if (verbose) cout << "Returning result " << (result.first ? "-> T <-" : "F") << endl;
        return result;
    }
private:
    /** Execution mutex */
    mutex one_at_a_time_;
};

/**
 * Function setup to illustrate getting work results from the Executor.  Two
 * futures are set up here (ordinarilly they would be syntastically chained,
 * but the isn't working here.)  The first promise starts the worker, the
 * second checks to see if the worker was successful, and if so, prints the
 * value. */
auto hammerMutex(Executor& worker, int start) -> boost::future<bool> {
    using namespace boost;

    auto f1 = async([start, &worker]() {
            // Start the worker
            return worker.Go(start);
        });
    auto f2 = f1.then([start](decltype(f1)& f) {
        // Grab the result of the work
        auto result = f.get();

        // Was the worker able to get the mutex?
        if (result.first) {
            cout << "Promise result = " << start << " -> " << *(result.second) << endl;
        } else {
            cout << "Worker was not able to get lock." << endl;
        }
        return true;
    });

    // Return the promise
    return f2;
}

int main(int argc, char *argv[]) {
    vector<boost::future<bool>> futures;

    // Turn on verbosity?
    if (argc > 1) verbose = true;

    // Create our worker.  Only one instance so it mimics a singleton.
    Executor worker;

    // Now, attempt to lunch a bunch of threads asynchronously
    for (int i=0; i<30; i++) {
        // Futures are used here rather than threads to ensure that the
        // future.get() functions were called, otherwise the program would
        // terminate without them executing.  This way, we create a future per
        // "thread", and later call `.get()` to get the result.  In this
        // example the result will always be true.
        //
        // The point of these lines is just to show that the mutex does block
        // out the majority of attemps to do work, but that some will still
        // make it through.
        futures.push_back(hammerMutex(worker, i*10));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(50));
    }

    // Make sure that the promises are fulfilled.  We don't care about the
    // result of this promise, but we need to ensure that it's fully run.
    for (auto& f : futures) f.get();
}

/* vim: set ts=4 sw=4 expandtab : */
