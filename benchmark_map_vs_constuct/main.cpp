#include <iostream>
#include <vector>
#include <Eigen/Core>

#include "Stopwatch/StopWatch.h"

struct point_t
{
    double     X;
    double     Y;
    double     Z;
    int64_t    timestamp;
    int8_t     intensity;
};

auto test_map_point(std::vector<point_t> const& points) -> double
{
    double something = 0;
    std::for_each(points.begin(), points.end(), [&something](point_t const& p)
    {
    using T = double const*;
        Eigen::Stride<sizeof(double), 0> stride{};

        using S = decltype(stride);
        Eigen::Map<
            const Eigen::Vector3d, /* Matrix type */
            Eigen::Unaligned,      /* MapOptions */
            S                      /* Stride Type */
        > ep(reinterpret_cast<T>(&p), 1, 3, stride);

        something += ep.x();
    });

    return something;
}

auto test_construct_point(std::vector<point_t> const& points) -> double
{
    double something = 0;
    std::for_each(points.begin(), points.end(), [&something](point_t const& p)
    {
        Eigen::Vector3d ep(p.X, p.Y, p.Z);
        something += ep.x();
    });

    return something;
}

auto main(int argc, char *argv[]) -> int
{
    std::vector<point_t> points;
    auto const n_points = 100000000;
    int ms_map{0}, ms_construct{0};
    double res = 0;

    for (int i = 0; i<n_points; i++)
    {
        points.emplace_back();
        points.back().X=1;
        points.back().Y=2;
        points.back().Z=3;
        points.back().timestamp = 10;
        points.back().intensity = char(15);
    }

    {
        StopWatch s;
        auto const val = test_map_point(points);
        ms_map = s.ElapsedMs();
        res += val;
    }

    {
        StopWatch s;
        auto const val = test_construct_point(points);
        ms_construct = s.ElapsedMs();
        res += val;
    }

    std::cout << res << "\n"; // I don't care, but I want to ensure this isn't
                              // optimized out by ignoring the results.

    std::cout << "Operation with map:        " << ms_map << " ms" << "\n";
    std::cout << "Operation with construct : " << ms_construct << " ms" << "\n";

    std::cout << "map/construct = " <<
        ((static_cast<float>(ms_map)/static_cast<float>(ms_construct))*10000.0f/100.0f)
        << "% (>100 means swap is slower)" << "\n";
    std::cout << "construct/map = " <<
        ((static_cast<float>(ms_construct)/static_cast<float>(ms_map))*10000.0f/100.0f)
        << "% (>100 means erase is slower)" << "\n";

    return 0;
}

/* vim: set ts=4 sw=4 sts=4 expandtab ffs=unix,dos : */
