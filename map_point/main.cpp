#include <iostream>
#include <type_traits>
#include <vector>
#include <Eigen/Core>

namespace lcmOPAL2
{
    class point_t
    {
       public:
        double     X;
        double     Y;
        double     Z;
        int64_t    timestamp;
        int8_t     intensity;
    };
}

auto main(int argc, char *argv[]) -> int
{
    std::vector<lcmOPAL2::point_t> points;

    points.emplace_back();
    points.back().X=1;
    points.back().Y=2;
    points.back().Z=3;
    points.back().timestamp = 10;
    points.back().intensity = char(15);

    points.emplace_back();
    points.back().X=10;
    points.back().Y=20;
    points.back().Z=30;
    points.back().timestamp = 100;
    points.back().intensity = char(150);

    for (auto const& p : points)
    {
        using T = std::add_pointer<std::add_const<decltype(lcmOPAL2::point_t::X)>::type>::type;
        Eigen::Stride<sizeof(lcmOPAL2::point_t::X), 0> stride{}; // Works

        using S = decltype(stride);
        Eigen::Map<
            const Eigen::Vector3d, /* Matrix type */
            Eigen::Unaligned,      /* MapOptions */
            S                      /* Stride Type */
        > ep(reinterpret_cast<T>(&p), 1, 3, stride);

        std::cout << " p = " << p.X << " " << p.Y << " " << p.Z << ", ep = " << ep.transpose() << "\n";
    }

    return 0;
}

/* vim: set ts=4 sw=4 sts=4 expandtab ffs=unix,dos : */
