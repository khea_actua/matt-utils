#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, CMake


class TargetTransformCalculatorConan(ConanFile):
    name        = 'map_point'
    version     = '1.0'
    license     = 'NTC'
    description = 'Testing using Eigen Map on an LCM point'
    settings    = 'os', 'compiler', 'build_type', 'arch'
    generators  = 'cmake', 'virtualenv'
    requires    = 'eigen/[>= 3.2.5]@ntc/stable'

    scm = {
        'type':      'git',
        'url':       'auto',
        'revision':  'auto'
    }

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy(pattern='bin/*')

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
