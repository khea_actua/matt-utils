///////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2014. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#include <Wt/WBootstrapTheme>

#include "WebSchedulerApplication.h"


#include <Wt/WTemplate>
#include <Wt/WString>
#include <Wt/WText>
#include <Wt/WApplication>
#include <Wt/WCheckBox>
#include <Wt/WComboBox>
#include <Wt/WValidator>
#include <Wt/WDate>
#include <Wt/WDateEdit>
#include <Wt/WDateValidator>
#include <Wt/WFormModel>
#include <Wt/WRegExpValidator>
#include <Wt/WString>
#include <Wt/WTemplate>
#include <Wt/WTemplateFormView>
#include <Wt/WText>
#include <Wt/WTimeEdit>
#include <Wt/WImage>

const Wt::WFormModel::Field SchedulerFormModel::TaskStartDateField = "task-start-date";
const Wt::WFormModel::Field SchedulerFormModel::TaskStartTimeField = "taskStartTime";


SchedulerFormModel::SchedulerFormModel(Wt::WObject *parent)
    : Wt::WFormModel(parent)
{
    // addField(Field, field-info)
    addField(TaskStartDateField);
    addField(TaskStartTimeField);

    setValidator(TaskStartDateField, createDateValidator());
    setValidator(TaskStartTimeField, createTimeValidator());

}

// Get the task data from the model
Wt::WString SchedulerFormModel::TaskData() {
    return
      "Start: " + Wt::asString(value(TaskStartDateField)) + " "
    + Wt::asString(value(TaskStartTimeField))
    + ";";
}


// Date Validator.  Date should be in the future
Wt::WValidator* SchedulerFormModel::createDateValidator() {
    auto v = new Wt::WDateValidator();
    v->setBottom(Wt::WDate::currentDate());
    v->setTop(Wt::WDate::currentDate().addMonths(2)); // Magic number, might want to set this somewhere
    v->setFormat("yyyy/MM/dd");
    v->setInvalidBlankText("Start date is required.");
    v->setInvalidNotADateText("Date should be entered in the format yyyy/MM/dd");
    v->setInvalidTooEarlyText("Start date must be in the future");
    v->setMandatory(true);
    return v;
}

// Simply apply a regex
Wt::WValidator* SchedulerFormModel::createTimeValidator() {
    //auto v = new Wt::WRegExpValidator("^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:[0-5]?[0-9])?$");
    auto v = new Wt::WTimeValidator("hh:mm");
    v->setMandatory(true);
    v->setInvalidBlankText("Start time is required!"); // place in template
    return v;
}

// inline constructor
SchedulerFormView::SchedulerFormView()
{
    model_ = new SchedulerFormModel(this);

    setTemplateText(tr("form-template"));

    //
    // Form-controls
    auto date_edit = new Wt::WDateEdit();
    date_edit->setEmptyText("YYYY/MM/dd");
    setFormWidget(SchedulerFormModel::TaskStartDateField, date_edit,
        [=] () { // updateViewValue()
            try
            {
                auto date = boost::any_cast<Wt::WDate>
                        (model_->value(SchedulerFormModel::TaskStartDateField));
                date_edit->setDate(date);
            }
            catch (const boost::bad_any_cast&)
            {
                Wt::log("debug") << "date_edit value is not valid date";
            }
        },
        [=] () { // updateModelValue()
            auto date = date_edit->date();
            model_->setValue(SchedulerFormModel::TaskStartDateField, date);
        }
    );

    auto time_te = new Wt::WTimeEdit();
    time_te->setFormat("hh:mm");
    setFormWidget(SchedulerFormModel::TaskStartTimeField, time_te);

    updateView(model_);
}

WebSchedulerApplication::WebSchedulerApplication(const Wt::WEnvironment &env)
    : Wt::WApplication(env)
{

    auto main_container = new SchedulerFormView();
    root()->addWidget(main_container);

}



Wt::WApplication* createApplication(const Wt::WEnvironment& env)
{
    // Create application
    auto app = new WebSchedulerApplication(env);

    //////////////////////////////////////////////////////////
    // Set up look & feel.  So, theme, resources, css, etc
    // This could be done here or in the constructor
    //////////////////////////////////////////////////////////


    // Config set to use progressive bootstrap
    // http://www.webtoolkit.eu/wt/doc/reference/html/overview.html#progressive_bootstrap
    auto bootstrapTheme = new Wt::WBootstrapTheme(app);
    bootstrapTheme->setVersion(Wt::WBootstrapTheme::Version3);
    bootstrapTheme->setResponsive(true);
    app->setTheme(bootstrapTheme);

    // load the default bootstrap3 (sub-)theme
    // Matt: Not sure this is necissary, doesn't seem to do anything,
    // but we're using the default theme anyways I imagine
    app->useStyleSheet("resources/themes/bootstrap/3/bootstrap-theme.min.css");

    // For better support for a mobile device. Note this requires
    // progressive bootstrap being enabled (see wt_config.xml).
    //
    // Currently the warning: "WApplication: WApplication::addMetaHeader() with no effect"
    // is thrown with or without this line.
    app->addMetaHeader("viewport", "width=device-width, initial-scale=1, maximum-scale=1");

    // Pull in our messages (html partials)
    app->messageResourceBundle().use(app->appRoot() + "template");

    app->setTitle("Validation Test");
    //app->setTitle(Wt::WString::tr("app-title")); // Doesn't seem to work yet

    //////////////////////////////////////////////////////////
    // /Set up look & feel.  So, theme, resources, css, etc
    //////////////////////////////////////////////////////////

    return app;
}

