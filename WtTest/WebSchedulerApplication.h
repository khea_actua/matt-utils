#ifndef WEBSCHEDULERAPPLICATION_H
#define WEBSCHEDULERAPPLICATION_H

#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WImage>
#include <Wt/WMenuItem>
#include <Wt/WTabWidget>
#include <Wt/WTimer>
#include <Wt/WApplication>
#include <Wt/WDateEdit>
#include <Wt/WStringListModel>
#include <Wt/WFormModel>
#include <Wt/WStandardItemModel>
#include <Wt/WString>
#include <Wt/WTemplateFormView>
#include <Wt/WValidator>


class WebSchedulerApplication : public Wt::WApplication
{
public:
    WebSchedulerApplication(const Wt::WEnvironment& env);

    /** This static function takes a label name and associated
     * widget, and adds it as a new row to the row template that
     * is passed in. row is pointed at a new template during
     * the function. */
    static Wt::WTemplate* SetupGridEntry(const std::string& label_name, Wt::WWidget* widget);

protected:

};


/**
 * @brief The SchedulerFormModel class Controls the bahaviour of all
 * the data required by the Scheduler form.  This is a composited
 * member of the SchedulerFormModel.
 */
class SchedulerFormModel : public Wt::WFormModel
{
public:
    // Associate each field with a unique string literal.
    static const Wt::WFormModel::Field TaskStartDateField;
    static const Wt::WFormModel::Field TaskStartTimeField;

    /**
     * @brief SchedulerFormModel Form model for the scheduler.  This object
     * is responsible for how all the elements on the form behave.
     * @param parent
     */
    SchedulerFormModel(Wt::WObject *parent = nullptr);

    /**
     * @brief TaskData Ensentially a serialize to string function.
     * Returns a string with all settings on the current task
     * @return
     */
    Wt::WString TaskData();


private:
    // Validators

    /**
     * @brief createTimeValidator Enforces time format hh:mm:ss
     * @return
     */
    Wt::WValidator* createTimeValidator();

    /**
     * @brief createDateValidator Enforces that date is in the future
     * @return
     */
    Wt::WValidator* createDateValidator();

};

/**
 * @brief The SchedulerFormView class Handles the UI/display of all the
 * form elements required by the Scheduler form.  Loads the template,
 * initilizes the form elements, handles events, etc.  It's composed
 * of the SchedulerFormModel which is the real controller (model) for
 * what we are trying to build with this form.
 */
class SchedulerFormView : public Wt::WTemplateFormView
{
public:
    /**
     * @brief SchedulerFormView This is the view for the scheduler form.
     * This object dictates how the form is displayed
     * @param session
     */
    SchedulerFormView();

private:
    SchedulerFormModel *model_;

};


/**
 * @brief Creates a new WT Web Application, adds CSS and JS
 * used throughout
 * @param env
 * @return
 */
Wt::WApplication* createApplication(const Wt::WEnvironment& env);

#endif // WEBSCHEDULERAPPLICATION_H
