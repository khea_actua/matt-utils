///////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2014. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#include "WebSchedulerApplication.h"

int main(int argc, char **argv)
{
    return Wt::WRun(argc, argv, &createApplication);
}
