////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "Applications/OPAL2/3DRiWebScheduler/ReturnCodes.h"

#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "SensorStatus.h"

using namespace OPAL2::Client3DRi;

enum ReturnCode {
    kSuccess=0,
    kError=-1
};

ReturnCode runExample(SensorIDType nSensorID);
ReturnCode alterSensorParameters(OPAL2Ptr& pOPAL2, COPAL2Parameters& params);

// Example program demostrating use of the 3DRi Client API
int main(int argc, char* argv[]) {

    OPAL2::Client3DRi::SensorIDType nSensorID = -1;

    if (argc >= 2 && argc <= 3) {
        nSensorID = boost::lexical_cast<int>(argv[1]);
    } else {
        std::cout << "Usage: " << argv[0] << " <sensor id>" << std::endl;
        return -1;
    }

	std::cout << "Sensor ID = " << nSensorID << std::endl;

    SensorStatus sensor_status{nSensorID};
    sensor_status.Run();
}

// vim: ts=4 sw=4 sts=4 expandtab :
