////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "MyStatusObserver.h"
#include <3DRiClient/Include/3DRiClientOPAL2.h>
#include <3DRiClient/Include/OPAL2Status.h>
#include <3DRiClient/Include/3DRiClientProperties.h>

using namespace OPAL2::Client3DRi;

CMyStatusObserver::CMyStatusObserver()
{
}

CMyStatusObserver::~CMyStatusObserver()
{
}

/// Observer callback
void CMyStatusObserver::StatusUpdate(const OPAL2::Client3DRi::OPAL2StatusPtr& status)
{
    // Handle status updates here
    int nNumProperties = status->GetPropertyCount();

    std::cout  << "Received status update for sensor " << sensor_->GetID() << std::endl;
	PropertyNameVector vectNames;
    status->GetAllPropertyNames(vectNames);
    for (PropertyNameVector::iterator it = vectNames.begin();
         it != vectNames.end();
         ++it)
    {
        std::string strValue;
        status->GetPropertyValue(*it, strValue);

        std::cout << (*it) << " = " << strValue << std::endl;

    }

    status_ = status;
    update_ = std::time(nullptr);

    std::cout << "." << std::endl;   // For now, just output something to show that a status message has arrived.
}

// vim: ts=4 sw=4 sts=4 expandtab :
