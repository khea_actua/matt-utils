////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#ifndef SENSORSTATUSEX_H
#define SENSORSTATUSEX_H

#include <iostream>

#include "Applications/OPAL2/3DRiWebScheduler/ReturnCodes.h"

#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <3DRiClient/Include/3DRiClientControl.h>
#include <3DRiClient/Include/3DRiClientOPAL2.h>
#include <3DRiClient/Include/3DRiClientPointSource.h>

#include "LcmChannelDefinitions.h" // used for OPAL2_HEARTBEAT_FREQUENCY

#include "MyStatusObserver.h"

using namespace OPAL2::Client3DRi;
using namespace OPAL2::web;


class SensorStatus {
public:

    /** Map containing sensor objects registered as observers */
    using SensorMap = std::map<SensorIDType, CMyStatusObserverPtr>;

	SensorStatus(SensorIDType sensor_id);

    WebSchedulerReturnCode Run();
    WebSchedulerReturnCode PollAvailableSensors();

protected:
    // We don't care about this right now
    WebSchedulerReturnCode AlterSensorParameters(COPAL2Parameters& params);

    // Obtain a reference to the one and only control object
    C3DRiClientControl& control = C3DRiClientControl::GetInstance();

    SensorIDType sensor_id_;

    /** Sensors registered as observers **/
    SensorMap observed_sensors_;
};

#endif // SENSORSTATUSEX_H

// vim: ts=4 sw=4 sts=4 expandtab :
