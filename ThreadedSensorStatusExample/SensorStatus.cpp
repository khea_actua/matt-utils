////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "SensorStatus.h"

#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>


#include "LcmChannelDefinitions.h" // used for opal2HEARTBEAT_FREQUENCY

#include "MyStatusObserver.h"

using namespace OPAL2::Client3DRi;
using namespace OPAL2::web;

SensorStatus::SensorStatus(SensorIDType sensor_id) {
    sensor_id_ = sensor_id;
}


WebSchedulerReturnCode SensorStatus::Run() {

    // Obtain version information in numeric form
    // This call can be made before initialization.
    VersionInfo_t version;
    EResultType err = ResultSuccess;
    err = control.QueryVersion( version );
    if (ResultSuccess != err) {
        std::cout << "Unable to query version [" << err << "]" << std::endl;
        return kErrorUnableToQueryVersion;
    }
    std::cout << "Client3DRi version " << version.major << "." << version.minor << "." << version.revision << "." << version.build << std::endl;

    // Alternately, the version can also be queried as a string.  The string contains the version
    // information.
    std::string strVersion;
    err = control.GetVersionString( strVersion );
    if (ResultSuccess != err) {
        std::cout << "Unable to get version string [" << err << "]" << std::endl;
        return kErrorUnableToQueryVersion;
    }
    std::cout << strVersion << std::endl;;

    // Initialize the control.
    // The address and port must match the values in the SystemManager XML configuration file but
    // can be changed to fit with the local network requirements.  If changing the default address,
    // choose a multicast address in the IPv4 Local Scope -- 239.255.0.0/16.
    std::string strUDPMulticastAddress("239.255.35.53");
    unsigned int uiUDPMulticastPort = 35053;

    // Use Time-to-live value of 0 if SensorManager is also running on the same machine.  This prevents data from being transmitted beyond the local computer.
    // A Time-to-live value of 1 will allow the entire subnet to see data.
    unsigned int uiUDPMulticastTTL = 0;

    // Large buffer size choosen to avoid loss of data
    unsigned int uiReceiveBufferSize = 20000000;

    err = control.Initialize(strUDPMulticastAddress, uiUDPMulticastPort, uiUDPMulticastTTL, uiReceiveBufferSize);
    if (ResultSuccess != err) {
        std::cout << "Unable to initialize [" << err << "]" << std::endl;
        return kErrorUnableToInitialize;
    }

    PollAvailableSensors();

    std::cout << "Requesting status" << std::endl;
    for (const auto& e : observed_sensors_) {
        // Get a copy of the OPAL2 scan parameters
        COPAL2Parameters params;
        err = e.second->sensor_->GetParameters(params, true);
        if (ResultSuccess != err) {
            std::cout << "[1] Unable to get sensor parameters [" << err << "]" << std::endl;
            return kErrorUnableToGetSensorParams;
        }
    }

    // Either pause or call a function like GetStatus that creates a block
    std::cout << "Pausing for " << (OPAL2_HEARTBEAT_FREQUENCY) << " ms for heartbeat" << std::endl;
    boost::this_thread::sleep(boost::posix_time::milliseconds(OPAL2_HEARTBEAT_FREQUENCY));

    // Don't care about this
    //alterSensorParameters(opal2, params);

    std::cout << "Cleaning up" << std::endl;
    // Clean up the control object before exiting.
    err = control.Cleanup();
    if (ResultSuccess != err) {
        std::cout << "Unable to shut down [" << err << "]" << std::endl;
        return kErrorUnableToShutdown;
    }

    return kSuccess;
}

WebSchedulerReturnCode SensorStatus::PollAvailableSensors() {
    EResultType err = ResultSuccess;
    DataSourcePtrVector vectDataSources;

    // Now attempt to get the API to access the data sources that are available.
    // If the 3DRi system is not running this will timeout and the subsequent call to get the
    // data source vectors will fail.

    // Give the 3DRi software system 3 seconds to be up and ready for the the API to detect
    // This call will never return success if the 3DRi software is not up and running or if
    // the network settings are preventing transmission of data via multicast UDP.
    boost::system_time const timeout = boost::get_system_time() + boost::posix_time::milliseconds(3000);
    while (control.PopulateDataSources() != ResultSuccess) {
        if (boost::get_system_time() >= timeout) {
            break;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }


    // Request a vector that contains shared pointers to all of the available data sources.
    err = control.GetDataSources( vectDataSources );
    if (ResultSuccess != err) {
        std::cout << "Unable to obtain list of data sources [" << err << "]" << std::endl;
        return kErrorCannotObtainSensorList;
    }


//  if (observed_sensors_.size() > 0) return kSuccess;

    std::cout << "Found " << vectDataSources.size() << " sensors" << std::endl;

    // Find the OPAL2 data source
    bool bFound = false;
    for (const auto& e : vectDataSources) {
        if (e->GetType() == OPAL2_SENSOR) {
            auto opal2 = boost::static_pointer_cast<C3DRiClientOPAL2, C3DRiClientDataSource>(e);
            // opal2 is a OPAL2Ptr, which is a typedef boost::shared_ptr<C3DRiClientOPAL2>

            if (opal2.get() == NULL) {
                std::cerr << "Failure converting OPAL2 sensor" << std::endl;
                continue;
            }

            if (opal2->GetID() == sensor_id_) {

                // Create a status observer that will receive status updates from the sensor in a separate thread
                // This observer simply prints out the status when it arrives.  See CMyStatusObserver::StatusUpdate()
                std::cout << "Registering an observer on the sensor" << std::endl;

                // Verify that the sensor is available
                if (!opal2->IsConnected()) {
                    std::cout << "Scanner is not connected." << std::endl;
                    return kErrorScannerNotConnected;
                }



                // Original to example, doesn't crash
                // boost::shared_ptr<CMyStatusObserver> pMyStatusObserver(new CMyStatusObserver());
                // opal2->RegisterStatusObserver(boost::static_pointer_cast<IOPAL2StatusObserver>(pMyStatusObserver));
                // opal2->UnregisterStatusObserver(); // Works here

                // ######################################################
                // Bridging working code and non-working code
                // CMyStatusObserver sensor_container;
                // sensor_container.sensor_ = opal2;
                // observed_sensors_.emplace(opal2->GetID(), sensor_container);
                //observed_sensors_[opal2->GetID()].sensor_ = opal2;

                //IOPAL2StatusObserverPtr status_observer{&observed_sensors_[opal2->GetID()]};

                CMyStatusObserverPtr pMyStatusObserver(new CMyStatusObserver());
                pMyStatusObserver->sensor_= opal2;
                observed_sensors_.emplace(opal2->GetID(), pMyStatusObserver);
                opal2->RegisterStatusObserver(boost::static_pointer_cast<IOPAL2StatusObserver>(pMyStatusObserver));
                opal2->UnregisterStatusObserver();

                // ######################################################

                // // This call will create the pair <id, SensorInterfaceStruct>, add it into
                // // the observed sensors map, and then set the memory variable.
                // observed_sensors_[opal2->GetID()].sensor_ = opal2;
                // IOPAL2StatusObserverPtr status_observer{&observed_sensors_[opal2->GetID()]}; // STACK Variable
                // opal2->RegisterStatusObserver(status_observer);
                // opal2->UnregisterStatusObserver();

                break;
            }
        }
    }

    return kSuccess;
}


WebSchedulerReturnCode SensorStatus::AlterSensorParameters(COPAL2Parameters& params) {
    EResultType err = ResultSuccess;/*{{{*/
    //
    // // For demonstration purposes, run through the parameters and output the current values and
    // // also the valid ranges for each parameter.  Note that ranges for items that are numeric
    // // are minimum and maximum and items that are lists include all valid list items.
    // OPAL2::Client3DRi::uint32_t uiNumParams = params.GetPropertyCount();
    // PropertyNameVector vectNames;
    // params.GetAllPropertyNames(vectNames);
    // for (PropertyNameVector::iterator it = vectNames.begin();
    //      it != vectNames.end();
    //      ++it)
    // {
    //     std::string strValue;
    //     params.GetPropertyValue(*it, strValue);
    //
    //     std::cout << (*it) << " = " << strValue << std::endl;
    //
    //     PropertyRangeVector vectRange;
    //     params.GetPropertyRange(*it, vectRange);
    //
    //     int i = 0;
    //     for (PropertyRangeVector::iterator itRange = vectRange.begin();
    //          itRange != vectRange.end();
    //          ++itRange)
    //     {
    //         std::cout << "    Range[" << i << "] = " << (*itRange) << std::endl;
    //         ++i;
    //     }
    // }
    //
    // // Again for demonstration purposes, set each of the scan parameters to some valid value for scanning.
    // params.SetPropertyValue("Scan_Duration", "6.0");
    // params.SetPropertyValue("Inner_Motor_RPS", "50.55");
    // params.SetPropertyValue("Outer_Motor_RPS", "15.743");
    // params.SetPropertyValue("Laser_Power", "25");
    // params.SetPropertyValue("Scan_Rate", "200 kHz");
    // params.SetPropertyValue("Detection_Mode", "Clear");
    // params.SetPropertyValue("Maximum_Edges", "1");
    // params.SetPropertyValue("Edge_Offset", "0");
    // params.SetPropertyValue("Edge_Gain", "1");
    // params.SetPropertyValue("Interpolate", "Yes");
    //
    // // Send the parameters back to the sensor
    // err = opal2->SetParameters(params);
    // if (ResultSuccess != err) {
    //     std::cout << "Unable to set sensor parameters [" << err << "]" << std::endl;
    //     return kError;
    // }
    //
    // // Get a copy of the parameters again to demonstrate that they have been updated.
    // // For demonstration purposes, not really required.
    // COPAL2Parameters paramsUpdated;
    // err = opal2->GetParameters(paramsUpdated,true);
    // if (ResultSuccess != err) {
    //     std::cout << "[2] Unable to get sensor parameters [" << err << "]" << std::endl;
    //     return kError;
    // }
    //
    // uiNumParams = paramsUpdated.GetPropertyCount();
    // paramsUpdated.GetAllPropertyNames(vectNames);
    // for (PropertyNameVector::iterator it = vectNames.begin();
    //      it != vectNames.end();
    //      ++it)
    // {
    //     std::string strValue;
    //     paramsUpdated.GetPropertyValue(*it, strValue);
    //
    //     std::cout << (*it) << " = " << strValue << std::endl;
    // }
    //
    // // Set up the parameters for a seconds scan, this one to be of unlimited duration (denoted by the 0.0 second Scan_Duration)
    // params.SetPropertyValue("Scan_Duration", "0.0");
    // params.SetPropertyValue("Inner_Motor_RPS", "30.1");
    // params.SetPropertyValue("Outer_Motor_RPS", "8.1");
    // err = opal2->SetParameters(params);
    // if (ResultSuccess != err) {
    //     std::cout << "Unable to set sensor parameters [" << err << "]" << std::endl;
    //     return kError;
    // }

/*}}}*/
}






// vim: ts=4 sw=4 sts=4 expandtab foldmethod=marker:
