////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <fstream>
#include <string>
#include <ctime>
#include <3DRiClient/Include/OPAL2StatusObserver.h>
#include <boost/thread/recursive_mutex.hpp>

/// Class used to monitor the arrival of data from the sensor
class CMyStatusObserver : public OPAL2::Client3DRi::IOPAL2StatusObserver
{
public:
    CMyStatusObserver();
    virtual ~CMyStatusObserver();

    /// Observer callback
    virtual void StatusUpdate(const OPAL2::Client3DRi::OPAL2StatusPtr& status);

    OPAL2::Client3DRi::OPAL2StatusPtr status_;
    OPAL2::Client3DRi::OPAL2Ptr sensor_;
    std::time_t update_;

protected:
    boost::recursive_mutex m_mutex;

private:

    /// Default copy constructor - not defined
    CMyStatusObserver( const CMyStatusObserver& );
    /// Default assignment operator - not defined
    CMyStatusObserver& operator=( const CMyStatusObserver& );
};

using CMyStatusObserverPtr = boost::shared_ptr<CMyStatusObserver>;

// vim: ts=4 sw=4 sts=4 expandtab :
