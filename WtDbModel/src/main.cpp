///////////////////////////////////////////////////////////////////////////////
// Copyright (c) Neptec Technologies Corporation, 2015. All rights reserved. //
///////////////////////////////////////////////////////////////////////////////

// #include <signal.h>

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/SqlConnection>
#include <Wt/Dbo/Session>

// #include <CommonLib/OPAL2Memory.hpp>

namespace dbo = Wt::Dbo;
using namespace std;

class Post;

class User : public dbo::Dbo<User> {
public:
    using Ptr = dbo::ptr<User>;

    User() {}

    User(const std::string& name)
        : name_(name) {}

    ~User() {}

    template<class Action>
    void persist(Action& a) {
        dbo::field(a, name_, "name");

        dbo::hasMany(a, posts_, dbo::ManyToOne, "user_posts");
    }

    std::string name_ = "";

    dbo::collection<dbo::ptr<Post>> posts_;

    // const std::string toString() const { return name_; };
};

class Post : public dbo::Dbo<Post> {
public:
    using Ptr = dbo::ptr<Post>;

    Post() {}
    Post(const std::string& title)
        : title_(title) {}
    ~Post() {}

    template<class Action>
    void persist(Action& a) {
        dbo::field(a, title_, "title");
        dbo::belongsTo(a, user_, "user_posts", dbo::OnDeleteCascade);
    }

    std::string title_ = "";

    User::Ptr user_;

    const std::string toString() const {
        return title_;
    };
};

int main(int argc, char **argv) {

    dbo::backend::Sqlite3 sqlite3(":memory:");
    // dbo::backend::Sqlite3 sqlite3("db");
    sqlite3.setProperty("show-queries", "true");

    dbo::Session session;
    session.setConnection(sqlite3);

    session.mapClass<User>("user");
    session.mapClass<Post>("post");

    try {
        session.createTables();
    } catch (Wt::Dbo::Exception& e) {
        cout << e.what() << endl;
    }

    dbo::Transaction trans1(session);
    auto matt = session.add(new User{"matt"});
    matt.modify()->posts_.insert(session.add(new Post{"Editorial on something"}));
    matt.modify()->posts_.insert(session.add(new Post{"Editorial on nothing"}));
    matt.modify()->posts_.insert(session.add(new Post{"Magnum Opus"}));

    // trans1.commit();

    auto tuser = session.add(new User{"testuser"});
    tuser.modify()->posts_.insert(session.add(new Post{"Manifesto"}));
    trans1.commit();

    using UserPost = boost::tuple<User::Ptr, Post::Ptr>;
    dbo::Transaction trans2(session);
    {
        auto posts = session.query<UserPost>("SELECT user, post FROM user INNER JOIN post ON user.id=post.posts_id")
           .resultList();

        cout << "\nUser Posts" << endl;
        for (auto up : posts) {
            auto user = boost::get<0>(up);
            auto post = boost::get<1>(up);
            cout << user->name_ << ": " << post->title_ << endl;
        }
    }
    trans2.commit();


    cout << "\nDeleting user matt" << endl;
    dbo::Transaction trans3(session);
    session.execute("DELETE FROM user WHERE name = 'matt'");
    trans3.commit();



    cout << "\nRe-reading posts" << endl;
    dbo::Transaction trans4(session);
    {
        auto posts = session.query<UserPost>("SELECT user, post FROM user INNER JOIN post ON user.id=post.posts_id")
           .resultList();

        cout << "\nUser Posts" << endl;
        for (auto up : posts) {
            auto user = boost::get<0>(up);
            auto post = boost::get<1>(up);
            cout << user->name_ << ": " << post->title_ << endl;
        }
    }

    trans4.commit();

    return 0;
}

/* vim: set ts=4 sw=4 sts=4 expandtab ffs=unix : */
