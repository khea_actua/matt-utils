// Benchmarking whether it's faster to erase from a list, or save good elements
// to a second list and swap at the end.  See other vector benchmarks at:
// https://www.acodersjourney.com/6-tips-supercharge-cpp-11-vector-performance/

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

#include "Stopwatch/StopWatch.h"

struct BigTestStruct
{
    BigTestStruct(int const _iValue)
        : iValue(_iValue)
    {}

    int iValue = 1;
    float fValue;
    long lValue;
    double dValue;
    char cNameArr[10];
    int iValArr[100];
};

auto remove_even_with_erase(std::vector<BigTestStruct>& v, std::function<bool(BigTestStruct const&)> predicate) -> void
{
    v.erase(std::remove_if(v.begin(), v.end(), predicate));
}

auto remove_even_with_copy_and_swap(std::vector<BigTestStruct>& v) -> void
{
    std::remove_reference<decltype(v)>::type filtered;
    for (const auto& i : v)
    {
        if (i.iValue % 2 == 0)
        {
            filtered.push_back(i);
        }
    }

    v.swap(filtered);
}

auto remove_swap_and_pop(std::vector<BigTestStruct>& v) -> void
{
    for (std::size_t i = 0; i<v.size(); ++i)
    {
        if (v[i].iValue % 2 == 0)
        {
            std::swap(v[i], v.back());
            v.pop_back();
        }
    }
}

auto gen(std::size_t const n) -> std::vector<BigTestStruct>
{
    std::vector<BigTestStruct> v;
    for (std::size_t i = 0; i<n; i++)
    {
        v.emplace_back(i);
    }

    return v;
}

auto main(int argc, char *argv[]) -> int
{
    std::size_t const n = 6000000;
    auto v2 = gen(n);

    int ms_erase = 0;
    int ms_erase_swaths = 0;
    int ms_swap = 0;
    int ms_swap_and_pop = 0;

    auto remove_evens = [](BigTestStruct const& i) { return i.iValue % 2 == 0; };
    auto remove_even_hundreds = [](BigTestStruct const& i) { return int(i.iValue/1000) % 2 == 0; };

    {
        auto v1 = gen(n);
        StopWatch s;
        remove_even_with_erase(v1, remove_evens);
        ms_erase = s.ElapsedMs();
    }

    {
        StopWatch s;
        remove_even_with_erase(v2, remove_even_hundreds);
        ms_erase_swaths = s.ElapsedMs();
    }

    {
        StopWatch s;
        remove_even_with_copy_and_swap(v2);
        ms_swap = s.ElapsedMs();
    }

    {
        auto v3 = gen(n);
        StopWatch s;
        remove_swap_and_pop(v3);
        ms_swap_and_pop = s.ElapsedMs();
    }

    std::cout << "Operation with erase:          " << ms_erase        << " ms" << "\n";
    std::cout << "Operation with erase swatches: " << ms_erase_swaths << " ms" << "\n";
    std::cout << "Operation with swap: "           << ms_swap         << " ms" << "\n";
    std::cout << "Operation with swap/pop: "       << ms_swap_and_pop << " ms" << "\n";

    std::cout << "swap/erase = " <<
        ((static_cast<float>(ms_swap)/static_cast<float>(ms_erase))*10000.0f/100.0f)
        << "% (>100 means swap is slower)" << "\n";
    std::cout << "erase/swap = " <<
        ((static_cast<float>(ms_erase)/static_cast<float>(ms_swap))*10000.0f/100.0f)
        << "% (>100 means erase is slower)" << "\n";

    return 0;
}


/* vim: set ts=4 sw=4 sts=4 expandtab ffs=unix,dos : */
