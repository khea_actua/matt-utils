cmake_minimum_required(VERSION 3.0.2)

project(vector_benchmark)

add_library(StopWatch Stopwatch/StopWatch.h Stopwatch/StopWatch.cpp)
target_include_directories(StopWatch PUBLIC ${CMAKE_SOURCE_DIR}/Stopwatch)
target_compile_features(StopWatch PUBLIC cxx_std_11)

add_executable(app main.cpp)
target_link_libraries(app PUBLIC StopWatch)

# vim: ts=4 sw=4 sts=0 expandtab ffs=unix,dos :
