Quick benchmark to test which method of filtering is faster:

1. `std::erase` with `std::remove_if`
1. Copying good elements to a second list and swapping them in after the filter
1. Swap bad elements to the end and pop (corrupts order)

This project uses [StopWatch](https://github.com/KjellKod/Stopwatch) to time (as a git submodule)

# Build

```sh
git submodule init
git submodule update

cd vector_benchmark
mkdir bld
cd bld

cmake -GNinja ..
ninja && ./app
```

# Results

```
n = 2000000
gcc   5.4: erase method is ~twice as fast. (46.1%)
clang 7.0: erase method is ~twice as fast. (42.2%)
```
