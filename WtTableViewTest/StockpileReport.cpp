#include "StockpileReport.h"

#include <Wt/WTableView>

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/QueryModel>

namespace matt
{

    StockpileReport::StockpileReport(Wt::Dbo::Session& session, Wt::WContainerWidget *parent)
        : session_(session), Wt::WContainerWidget(parent)

{
    auto table_view = new Wt::WTableView();
    auto model = new Wt::Dbo::QueryModel<Wt::Dbo::ptr<StockpileResult>>();
    model->setQuery(session.find<StockpileResult>());
    model->setBatchSize(10);

    model->addColumn("label", tr("stockpile-report-col-stockpile"));
    model->addColumn("volume", tr("stockpile-report-col-volume"));
    //model->addColumn("data_collected_datetime", tr("report-col-data"));

    table_view->setModel(model);

    // treat first column as 'fixed' row headers
    //table_view->setRowHeaderCount(1);

    // Enable sorting
    table_view->setSortingEnabled(true);

    // Set a default sort
    table_view->sortByColumn(1, Wt::DescendingOrder); //volume

    table_view->setSelectionMode(Wt::SingleSelection);
    //table_view->addStyleClass("table form-inline report-table table-striped"); // Doesn't actually get applied to the table
    //table_view->setOverflow(Wt::WContainerWidget::OverflowAuto);

    table_view->setHeight(300);


    // Attempted as a WTemplate and as a WContainerWidget, virtual scroll
    // was not enabled in either.
    addWidget(table_view);

    /*
    // Ugly JS hack to apply bootstrap classes to table rather than
    // widget containing the table.
    // Doesn't work when 'next' page is loaded
    auto widget_id = table_view->id();
    doJavaScript("$('#" + widget_id + " table').addClass('table report-table table-striped').removeClass('Wt-plaintable')");
    */

}

} // namespace matt
