#ifndef STOCKPILEREPORT_H
#define STOCKPILEREPORT_H

#include <Wt/WContainerWidget>
#include <Wt/Dbo/Session>

#include "TaskData.h"

namespace matt
{

class StockpileReport : public Wt::WContainerWidget
{
public:
    StockpileReport(Wt::Dbo::Session& session, Wt::WContainerWidget* parent = nullptr);
protected:

    /** This member represents the database session, and allows us
     * to interact with the database. */
    Wt::Dbo::Session& session_;

};

} // namespace matt

#endif
