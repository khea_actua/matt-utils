#include <Wt/WBootstrapTheme>

#include "WtWTableViewTest.h"
#include "TaskData.h"
#include "StockpileReport.h"

#include <Wt/WEnvironment>
#include <Wt/WTemplate>
#include <Wt/WText>
#include <Wt/WApplication>
#include <random>

namespace matt
{

WtWTableViewTest::WtWTableViewTest(const Wt::WEnvironment &env)
    : Wt::WApplication(env)
{

    //////////////////////////////////////////////////////////
    // Set up look & feel.  So, theme, resources, css, etc
    // This could be done here or in the constructor
    //////////////////////////////////////////////////////////

   /*
    // Config set to use progressive bootstrap
    // http://www.webtoolkit.eu/wt/doc/reference/html/overview.html#progressive_bootstrap
    auto bootstrapTheme = new Wt::WBootstrapTheme(this);
    bootstrapTheme->setVersion(Wt::WBootstrapTheme::Version3);
    bootstrapTheme->setResponsive(true);
    setTheme(bootstrapTheme);

    // load the default bootstrap3 (sub-)theme
    // Matt: Not sure this is necissary, doesn't seem to do anything,
    // but we're using the default theme anyways I imagine
    useStyleSheet("resources/themes/bootstrap/3/bootstrap-theme.min.css");

    // For better support for a mobile device. Note this requires
    // progressive bootstrap being enabled (see wt_config.xml).
    //
    // Currently the warning: "WApplication: WApplication::addMetaHeader() with no effect"
    // is thrown with or without this line.
    addMetaHeader("viewport", "width=device-width, initial-scale=1, maximum-scale=1");
*/
    // Pull in our messages (html partials)
    messageResourceBundle().use(appRoot() + "template");

    setTitle(Wt::WText::tr("app-title"));

    //////////////////////////////////////////////////////////
    // /Set up look & feel.  So, theme, resources, css, etc
    //////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    // Establish a connection to the database:
    //////////////////////////////////////////////////////////////

    session_.setConnection(sqlite3_);

    // Map the data objects to database tables:
    session_.mapClass<StockpileResult>("stockpile_results");

    // Try creating the database tables. This function throws
    // if there's an error.
    try
    {
        Wt::log("debug") << "Creating table schema";
        session_.createTables();
    }
    catch (Wt::Dbo::Exception& e)
    {
        // Annoying!  Uncomment during production, or learn how to use
        // a db namespace on the logger.
        Wt::log("debug") << e.what() << "\nUsing exiting database";
    }

     // Insert a bunch of entries.
    auto transaction = Wt::Dbo::Transaction{session_};

    auto rand_vol=std::bind(std::uniform_int_distribution<>{200,600}, std::default_random_engine{});
 
    for (int i = 0; i<20; i++) {
         auto sp = new StockpileResult();
         sp->label_="Matt " + std::to_string(i);
         sp->volume_= rand_vol();
         auto sp_ptr = session_.add(sp);
    }

    transaction.commit();


    auto main_container = new StockpileReport(session_);
    root()->addWidget(main_container);
}

} // namespace matt


Wt::WApplication* createApplication(const Wt::WEnvironment& env)
{
    // Create application
    auto app = new matt::WtWTableViewTest(env);
    return app;
}

