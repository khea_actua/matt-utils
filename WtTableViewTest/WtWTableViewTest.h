#ifndef WtWTableVIEWTEST
#define WtWTableVIEWTEST

#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WStandardItemModel>
#include <Wt/WString>
#include <Wt/WStringListModel>
#include <Wt/WTemplateFormView>

#include <Wt/Dbo/Session>
#include <Wt/Dbo/ptr>
#include <Wt/Dbo/backend/Sqlite3>


namespace matt
{

class WtWTableViewTest : public Wt::WApplication
{
public:
    WtWTableViewTest(const Wt::WEnvironment& env);

protected:

    /** This member specifies our database backend, Sqlite3. */
    Wt::Dbo::backend::Sqlite3 sqlite3_{"scan_schedule.db"};

    /** This member represents the database session, and allows us
     * to interact with the database. */
    Wt::Dbo::Session session_;


};

} //namespace matt

/**
 * @brief Creates a new WT Web Application, adds CSS and JS
 * used throughout
 * @param env
 * @return
 */
Wt::WApplication* createApplication(const Wt::WEnvironment& env);

#endif // WtWTableVIEWTEST
