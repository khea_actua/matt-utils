Matt Sandbox.

Contents:
- WtTest: Little standalone Wt project that builds against the vendor repo.  Use 'make' to compile, 'make run' to run. (Doesn't connect to cmakw in the repo in any way)
- Other Wt Tests
- MongoDB test
- Futures and Promises test